" General Config
set number
set wildmenu
set autoindent
set ts=4 sw=4
set ruler
set showmatch
set backspace=indent,eol,start
set visualbell
set autoread
set gcr=a:blinkon0
set undolevels=500

" Searching
set incsearch
set hlsearch
set ignorecase
set smartcase
nnoremap <leader><space> :nohlsearch<CR>

" Disables backups
set nobackup
set nowb
set noswapfile

" Colorschemes
colo aurora
syntax on

" Run makefile
map <F5> :make
 
" Remap Esc   
inoremap <S-Tab> <esc>
onoremap <S-Tab> <Esc>

" Tabs
noremap <C-Tab> :tabn

" No cheating :)
set mouse-=a
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

